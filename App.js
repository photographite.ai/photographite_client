import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import CacheStore from 'react-native-cache-store';
import _ from 'lodash'

import getStore from './src/redux/reducers/store';

const { store, AppWithNavigationState } = getStore()

// import {Navigator, middleware} from './src/router/Navigator';
// const store = createStore(reducers, applyMiddleware(middleware))

class App extends Component {
  checkToken() {
    CacheStore.get("userToken").then(value => {
      if (_.isEmpty(value)) {
        store.dispatch({ type: 'Navigation/NAVIGATE', routeName: 'UnAuthNavigation'})
      } else {
        store.dispatch({ type: 'Navigation/NAVIGATE', routeName: 'AuthNavigation'})
      }
    });
  }

  componentWillMount() {
    this.checkToken();
  }

  render(){
    return (
      <View style={{ flex: 1}}>
        <Provider store={store}>
          <AppWithNavigationState/>
        </Provider>
      </View>
    )
  }
}

export default App;
