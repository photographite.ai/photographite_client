import { connect } from 'react-redux';
import Signup from "./Signup.screen";

import { createUser, confirmPasswordChanged, passwordChanged, usernameChanged, phoneNumberChanged } from '../../redux/actions/user'


const mapStateToProps = state => {
    const { username, password, data, loading, phone } = state.userReducer;
    return {
        username,
        password,
        phone,
        data,
        loading
    }
}

// const mapDispatchToProps = dispatch => ({
//     onSignUp: payload => x  => onSignUp({ dispatch, payload, x })
// })

export default connect(mapStateToProps, {
    createUser, 
    confirmPasswordChanged, 
    passwordChanged, 
    usernameChanged,
    phoneNumberChanged
})(Signup);
