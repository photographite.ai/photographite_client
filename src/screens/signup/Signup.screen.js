import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { Item, Input, Content } from "native-base";

import styles from "./Signup.style";

class Signup extends Component {
  state = {
    formPhoneSelected: true
  };

  onPressTitle = obj => () => {
    this.setState(obj);
  };

  onPressLogin = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  onPressNext = () => {
    if (this.state.formPhoneSelected) {
      this.setState({ formPhoneSelected: false });
    } else {
      const { username, password, rePassword, phone } = this.props;
      this.props.createUser({ username, password, rePassword, phone });
    }
  };

  getActiveBorderColor = params => {
    if (params) {
      return {
        borderBottomColor: "#000"
      };
    } else {
      return {
        borderBottomColor: "#dedede"
      };
    }
  };

  getActiveTitleColor = params => {
    if (params) {
      return {
        color: "#000"
      };
    } else {
      return {
        color: "#999"
      };
    }
  };

  onChangeUsername = username => this.props.usernameChanged(username);

  onChangePassword = password => this.props.passwordChanged(password);

  onChangeRePassword = rePassword =>
    this.props.confirmPasswordChanged(rePassword);

  onChangePhoneNumber = phone => this.props.phoneNumberChanged(phone);

  renderFormPhone = () => {
    const { phone } = this.props;
    return (
      <View style={styles.textInputWraper}>
        <Item>
          <Text style={styles.phonePrefix}>ID +62</Text>
          <View style={styles.sparatorPhone} />
          <Input
            keyboardType="phone-pad"
            placeholder="Phone number"
            onChangeText={this.onChangePhoneNumber}
            value={phone}
          />
        </Item>
      </View>
    );
  };

  renderFormEmail = () => {
    const { username, password, rePassword } = this.props;
    return (
      <View>
        <View style={styles.textInputWraper}>
          <Input
            placeholder="Username"
            onChangeText={this.onChangeUsername}
            value={username}
          />
        </View>
        <View style={styles.textInputWraper}>
          <Input
            placeholder="Password"
            onChangeText={this.onChangePassword}
            value={password}
            secureTextEntry
          />
        </View>
        <View style={styles.textInputWraper}>
          <Input
            placeholder="Confirm Password"
            onChangeText={this.onChangeRePassword}
            value={rePassword}
            secureTextEntry
          />
        </View>
      </View>
    );
  };

  renderMultipleTitle = () => {
    const { formPhoneSelected } = this.state;

    return (
      <View style={styles.multipleTitleWraper}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={this.onPressTitle({ formPhoneSelected: true })}
          style={[
            styles.titleItem,
            this.getActiveBorderColor(formPhoneSelected)
          ]}
        >
          <Text
            style={[
              styles.textTitle,
              this.getActiveTitleColor(formPhoneSelected)
            ]}
          >
            TELEPHONE
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          onPress={this.onPressTitle({ formPhoneSelected: false })}
          style={[
            styles.titleItem,
            this.getActiveBorderColor(!formPhoneSelected)
          ]}
        >
          <Text
            style={[
              styles.textTitle,
              this.getActiveTitleColor(!formPhoneSelected)
            ]}
          >
            USERNAME
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { formPhoneSelected } = this.state;
    return (
      <View style={styles.container}>
        <Content contentContainerStyle={styles.scrollContainer}>
          <View style={styles.logo} />
          {this.renderMultipleTitle()}
          {formPhoneSelected ? this.renderFormPhone() : this.renderFormEmail()}
          <TouchableOpacity onPress={this.onPressNext}>
            <View style={styles.button}>
              {this.props.loading ? (
                <ActivityIndicator color="#6499ef" size="large" />
              ) : (
                <Text style={styles.textLogin}>NEXT</Text>
              )}
            </View>
          </TouchableOpacity>
          <View style={styles.footer}>
            <Text style={styles.footerText}>
              Already have an account?{" "}
              <Text onPress={this.onPressLogin} style={styles.footerTextBold}>
                Log In.
              </Text>
            </Text>
          </View>
        </Content>
      </View>
    );
  }
}

export default Signup;
