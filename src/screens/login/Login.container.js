import Login from "./Login.screen";
import { connect } from "react-redux";

import {
  postLogin,
  getLoginBegin,
  getLoginFailure,
  passwordChanged,
  usernameChanged
} from "../../redux/actions/user";

const mapStateToProps = state => {
  const { email, password, loading } = state.userReducer;
  return {
    email,
    password,
    loading
  };
};

export default connect(
  mapStateToProps,
  {
    postLogin,
    getLoginBegin,
    getLoginFailure,
    passwordChanged,
    usernameChanged
  }
)(Login);
