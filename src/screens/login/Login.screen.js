import React, { Component } from "react";
import {
  TextInput,
  TouchableOpacity,
  Text,
  ImageBackground,
  View,
  ActivityIndicator
} from "react-native";
import _ from "lodash";

import styles from "../login/Login.style";

import { Button, Content } from "native-base";

class Login extends Component {
  state = {
    username: "",
    password: "",
    transparent: true
  };

  onUsernameChanged = username => this.setState({ username });

  onPasswordChanged = password => this.setState({ password });

  onPressLogin = () => {
    const { username, password } = this.state;
    this.props.postLogin({ username, password });
  };

  onPressRegister = () => {
    const { navigation } = this.props
    navigation.navigate("Signup")
  }

  render() {

    return (
      <View style={styles.container}>
        <Content contentContainerStyle={styles.scrollContainer}>
          <Text style={styles.title}>Photographite</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Username"
            placeholderTextColor="#999"
            onChangeText={this.onUsernameChanged}
            value={this.state.username}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Password"
            placeholderTextColor="#999"
            onChangeText={this.onPasswordChanged}
            value={this.state.password}
            secureTextEntry
            autoCapitalize='none'
          />
          <TouchableOpacity onPress={this.onPressLogin}>
            <View style={styles.button}>
              {this.props.loading ? (
                <ActivityIndicator size='large' color={"#6499ef"}/>
              ) : (
                <Text style={styles.textLogin}>Log In</Text>
              )}
            </View>
          </TouchableOpacity>
          <View style={styles.footer}>
            <Text style={styles.footerText}>
              Dont have any account yet?{" "}
              <Text
                onPress={this.onPressRegister}
                style={styles.footerTextBold}
              >
                Create Account
              </Text>
            </Text>
          </View>
        </Content>
      </View>
    );
  }
}

export default Login;
