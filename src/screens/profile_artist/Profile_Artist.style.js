import { StyleSheet, Dimensions } from 'react-native';

const getWidth = Dimensions.get('window').width*0.3

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    row: {
        flexDirection: 'row'
    },
    avatar: {
        height: 100,
        width: 100,
        borderRadius: 50,
        alignSelf: 'center',
        marginVertical: 10
    },
    username: {
        fontSize: 30,
        color: '#000',
        fontWeight: '500',
        alignSelf: 'center',
        marginBottom: 20
    },
    textPost: {
        fontSize: 20
    },
    textSubscriber: {
        fontSize: 20
    },
    photo_image: {
        height: getWidth,
        width: getWidth,        
        margin: 3        
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    number: {
        fontSize: 35,
        color: '#000',
        fontWeight: '200'
    }
})

