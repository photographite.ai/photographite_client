import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Image, 
    FlatList, 
    Dimensions, 
    ScrollView,
    TouchableOpacity 
} from 'react-native';
import Header from '../../components/Header';
import { styles } from './Profile_Artist.style';

const getMargin = Dimensions.get('window').width * 0.18
const Data = {
    user_photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3fLR8TNQDwEwzpXR_HMcIIbbSq6hp2_MNpGoRbEMGOaseM9E_YQ',
    username: 'yusuf.adinata',
    subscribed: 10,
    photo_image: [
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJyNih2-BcOwOUgQLVUKmFg-Gypxmv1NueBoxDZ2GRk7D9pUwm'
        },
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQM4ogXSfsukvAtPnVqoUC1PI0d37jiBDLwLCVuTysAKW7GhnCInQ'
        },
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRG440k08pb1dc2GaNPSMlX-qEirvxzS1aQN72S3pBWC2R8PX9hKg'
        },
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyPOxzuZTrpKsDuGAftCBx3bp0AmvEsKNAbWh0Dl4I1sNcWKTy'
        },
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJyNih2-BcOwOUgQLVUKmFg-Gypxmv1NueBoxDZ2GRk7D9pUwm'
        },
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSB-zQpprzU4zRYHflpERBiOIxUgcpEzen-PcJUG4cXs89IEQ3mWw'
        },
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTtEgaZry0UJnxKb5wkajXqg5H9_TZpfj9-PJx5_1Xo674h509fg'
        },
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLtCrJgx5xgT7bq-I4U2sIT1jSIaDFI7C59eFXKVefQ0JDGZCH'
        },
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmCThEB6KUSuI1b5gOISsDIGKSh10h1fOJg3okKLZPdTyggxq6'
        },
        {
            id_post: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJyNih2-BcOwOUgQLVUKmFg-Gypxmv1NueBoxDZ2GRk7D9pUwm'
        }
    ]
}

class Profile_Artist extends Component {
    _renderItem = ({ item }) => {
        return (
            <TouchableOpacity>
                <Image
                    source={{ uri: item.id_post }}
                    style={styles.photo_image}
                />
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Header />
                <ScrollView>
                    <Image source={{ uri: Data.user_photo }} style={styles.avatar} />
                    <Text style={styles.username}>{Data.username}</Text>
                    <View style={[styles.row, { marginHorizontal: getMargin, justifyContent: 'space-between' }]}>
                        <View style={styles.center}>
                            <Text style={styles.textPost}>POST</Text>
                            <Text style={styles.number}>{Data.photo_image.length}</Text>
                        </View>
                        <View style={styles.center}>
                            <Text style={styles.textSubscriber}>SUBSCRIBER</Text>
                            <Text style={styles.number}>10</Text>
                        </View>
                    </View>
                    <View style={{ margin: 8 }}>
                        <FlatList
                            data={Data.photo_image}
                            keyExtractor={(x, i) => i.toString()}
                            renderItem={this._renderItem}
                            numColumns={3}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default Profile_Artist;
