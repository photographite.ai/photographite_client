import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    row: {
        flexDirection: 'row'
    },
    avatar: {
        height: 50,
        width: 50,
        borderRadius: 25,
        margin: 8
    },
    post_image: {
        height: 40,
        width: 40,
        margin: 8
    },
    username: {
        fontSize: 16,
        color: '#000',
        fontWeight: '500'
    },
    phone: {
        fontSize: 16
    }
})