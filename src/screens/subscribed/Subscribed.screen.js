import React, { Component } from "react";
import { View, Text, FlatList, Image, TouchableOpacity } from "react-native";
import HeaderSubsList from "../../components/HeaderSubsList";
import { styles } from "../subscribed/Subscribed.style";

const Data = [
  {
    user_id: 1,
    username: "yusuf.adinata",
    phone: "about",
    photo_user:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3fLR8TNQDwEwzpXR_HMcIIbbSq6hp2_MNpGoRbEMGOaseM9E_YQ",
    post_image: [
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQM4ogXSfsukvAtPnVqoUC1PI0d37jiBDLwLCVuTysAKW7GhnCInQ"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRG440k08pb1dc2GaNPSMlX-qEirvxzS1aQN72S3pBWC2R8PX9hKg"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyPOxzuZTrpKsDuGAftCBx3bp0AmvEsKNAbWh0Dl4I1sNcWKTy"
      }
    ]
  },
  {
    user_id: 2,
    username: "umar2731",
    phone: "about",
    photo_user:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSCeE-T_bLKN5wyzQ3vLt0vFQjjeW7Vtj_ieWTc3AzQKDADvarK",
    post_image: [
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSB-zQpprzU4zRYHflpERBiOIxUgcpEzen-PcJUG4cXs89IEQ3mWw"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTtEgaZry0UJnxKb5wkajXqg5H9_TZpfj9-PJx5_1Xo674h509fg"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLtCrJgx5xgT7bq-I4U2sIT1jSIaDFI7C59eFXKVefQ0JDGZCH"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmCThEB6KUSuI1b5gOISsDIGKSh10h1fOJg3okKLZPdTyggxq6"
      }
    ]
  },
  {
    user_id: 3,
    username: "ali_syahidin",
    phone: "about",
    photo_user:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhyToPhh7Pm8a0MNQs6SuLnAm82wbq6aDPPntWXAtStC5oPCVP",
    post_image: [
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJyNih2-BcOwOUgQLVUKmFg-Gypxmv1NueBoxDZ2GRk7D9pUwm"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpga7Z5C68upu8ClD0prTWk4eW0f28gvYUU13ATZjFg3xWDr9-"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgtSy13-meurMMyq2J5VfE9yXvNpe8pIIc1DP_b5jZdwRsOnrW"
      }
    ]
  },
  {
    user_id: 4,
    username: "agung",
    phone: "about",
    photo_user:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3fLR8TNQDwEwzpXR_HMcIIbbSq6hp2_MNpGoRbEMGOaseM9E_YQ",
    post_image: [
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQM4ogXSfsukvAtPnVqoUC1PI0d37jiBDLwLCVuTysAKW7GhnCInQ"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRG440k08pb1dc2GaNPSMlX-qEirvxzS1aQN72S3pBWC2R8PX9hKg"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyPOxzuZTrpKsDuGAftCBx3bp0AmvEsKNAbWh0Dl4I1sNcWKTy"
      }
    ]
  },
  {
    user_id: 5,
    username: "ridwan.khasanah",
    phone: "about",
    photo_user:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSCeE-T_bLKN5wyzQ3vLt0vFQjjeW7Vtj_ieWTc3AzQKDADvarK",
    post_image: [
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSB-zQpprzU4zRYHflpERBiOIxUgcpEzen-PcJUG4cXs89IEQ3mWw"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTtEgaZry0UJnxKb5wkajXqg5H9_TZpfj9-PJx5_1Xo674h509fg"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLtCrJgx5xgT7bq-I4U2sIT1jSIaDFI7C59eFXKVefQ0JDGZCH"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmCThEB6KUSuI1b5gOISsDIGKSh10h1fOJg3okKLZPdTyggxq6"
      }
    ]
  },
  {
    user_id: 6,
    username: "yahya,hanif",
    phone: "about",
    photo_user:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhyToPhh7Pm8a0MNQs6SuLnAm82wbq6aDPPntWXAtStC5oPCVP",
    post_image: [
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJyNih2-BcOwOUgQLVUKmFg-Gypxmv1NueBoxDZ2GRk7D9pUwm"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpga7Z5C68upu8ClD0prTWk4eW0f28gvYUU13ATZjFg3xWDr9-"
      },
      {
        id_post:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgtSy13-meurMMyq2J5VfE9yXvNpe8pIIc1DP_b5jZdwRsOnrW"
      }
    ]
  }
];

class Subscribed extends Component {
  renderItem = ({ item }) => {
    return (
      <View>
        <View style={styles.row}>
          <Image source={{ uri: item.photo_user }} style={styles.avatar} />
          <View>
            <Text style={[styles.username, { marginTop: 8 }]}>
              {item.username}
            </Text>
            <Text style={styles.phone}>{item.phone}</Text>
          </View>
        </View>
        <FlatList
          style={{ marginLeft: 70 }}
          data={item.post_image}
          horizontal
          keyExtractor={(x, i) => i.toString()}
          renderItem={this.renderItemImage}
        />
      </View>
    );
  };

  renderItemImage = ({ item }) => {
    return (
      <TouchableOpacity>
        <Image source={{ uri: item.id_post }} style={styles.post_image} />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderSubsList title={'Subscribed'}/>
        <FlatList
          data={Data}
          keyExtractor={(x, i) => i.toString()}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

export default Subscribed;
