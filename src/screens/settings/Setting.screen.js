import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import CacheStore from 'react-native-cache-store'

import styles from "./Setting.style";

class Setting extends React.Component {
  signOut(){
    CacheStore.remove('userToken')
    this.props.navigation.navigate('Landing')
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.signOutView}
          onPress={this.signOut.bind(this)}
        >
          <Text style={styles.textSignOut}>SIGN OUT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Setting;
