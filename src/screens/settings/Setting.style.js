import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  signOutView: {
    position: "absolute",
    backgroundColor: 'red',
    height: 50,
    width: '100%',
    justifyContent: 'center',
    alignItems: "center",
    bottom: 0
  },
  textSignOut: {
    color: 'white',
    fontSize: 15
  }
})
