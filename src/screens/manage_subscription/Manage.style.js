import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    backIcon: {
        height: 30,
        width: 30,
        margin: 8
    },
    button: {
        height: 40,
        width: '40%',
        backgroundColor: 'red',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton: {
        color: '#fff',
        fontSize: 16
    },
    rowList: { 
        borderBottomColor: '#f1f1f1', 
        borderBottomWidth: 1, 
        paddingBottom: 10, 
        justifyContent: 'space-between', 
        marginTop: 10,
        marginHorizontal: 8 
    },
    textBack: {
        fontSize: 25,
        marginLeft: 8        
    },
    artist: {
        fontSize: 18,
        color: '#000',
        fontWeight: '200'
    }
})