import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import { styles } from './Manage.style';

const backIcon = require('../../assets/back-icon.png')

const Data = [
    {
        'artist': 'Ridwan Khasanah',
        'subscribed': 'Unsubscribed'
    },
    {
        'artist': 'Umar',
        'subscribed': 'Unsubscribed'
    },
    {
        'artist': 'Tilis Tiadi',
        'subscribed': 'Unsubscribed'
    },
    {
        'artist': 'Yahya Hanif',
        'subscribed': 'Unsubscribed'
    }
]

class Manage extends Component {
    renderItem = ({ item }) => {
        return (
            <View style={[styles.row, styles.rowList]}>
                <Text style={styles.artist}>{item.artist}</Text>
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.textButton}>Unsubscribe</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    style={{ marginTop: 20 }}
                    data={Data}
                    keyExtractor={(x, i) => i.toString()}
                    renderItem={this.renderItem}
                />
            </View>
        )
    }
}

export default Manage;
