import Feed from "./Feed.screen";
import { connect } from "react-redux";

import { feedAction } from "../../redux/actions/feed";

const mapStateToProps = state => {
  const { data, loading } = state.feed;
  return {
    data,
    loading
  };
};

const mapDispatchToProps = dispatch => ({
  feedAction: () => dispatch(feedAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Feed);
