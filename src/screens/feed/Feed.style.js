import { StyleSheet, Dimensions } from "react-native";

const getHeight = Dimensions.get("window").height * 0.3;


export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 20,
    margin: 8
  },
  usernameText: {
    fontSize: 17,
    color: "#000",
    fontWeight: "500"
  },
  photoPost: {
    height: getHeight,
    flex: 1,
    marginVertical: 8
  },
  description: {
    marginLeft: 8,
    marginBottom: 8
  },
  iconRow: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    marginTop: 10,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  heartIcon: {
    width: 20,
    height: 20,
  },
  overlay: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  overlayHeart: {
    tintColor: '#fff',
  },
});
