import React, { Component } from "react";
import { View, FlatList, ActivityIndicator } from "react-native";
import moment from 'moment'

import FeedItem from "../../components/FeedItem";
import Header from "../../components/Header";
import { styles } from "../feed/Feed.style";

class Feed extends Component {
  state = {
    showMore: true,
  };
  
  componentWillMount() {
    this.props.feedAction();
  }

  onShowMore = () => {
    this.setState({
      showMore: false
    });
  };

  _renderItem = ({ item }) => {
    // story = Description ( front-end )
    // uploaded = Post time ( front-end )
    // image = Photo ( front-end )
    // author = { username, id, phone } ( front-end )

    const { author } = item;

    return (
      <FeedItem
        description={item.story}
        title={author.username}
        uri={item.image}
        time={moment(item.uploaded).format('MMMM Do YYYY')}
        photo={author.username.substring(0,2).toUpperCase()}
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          camera={true}
          onPress={() => this.props.navigation.navigate("UploadImage")}
          title={"Photographite"}
        />
        <FlatList
          refreshing={this.props.loading}
          onRefresh={this.props.feedAction}
          data={this.props.data}
          keyExtractor={(x, i) => i.toString()}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }
}

export default Feed;
