import { StyleSheet, Dimensions } from "react-native";

const getWidth = Dimensions.get("window").width * 0.3;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  row: {
    flexDirection: "row"
  },
  avatarAdd: {
    height: 25,
    width: 25,
    borderRadius: 12.5,
    backgroundColor: "purple",
    position: "absolute",
    top: 70,
    right: 0
  },
  avatarCoin: {
    height: 35,
    width: 35,
    alignSelf: "flex-end",
    marginRight: 8,
    marginTop: 8
  },
  avatar: {
    height: 100,
    width: 100,
    borderRadius: 50,
    alignSelf: "center",
    marginBottom: 10
  },
  username: {
    fontSize: 30,
    color: "#000",
    fontWeight: "500",
    alignSelf: "center",
    marginBottom: 20
  },
  textPost: {
    fontSize: 20
  },
  textSubscriber: {
    fontSize: 20
  },
  photo_image: {
    height: getWidth,
    width: getWidth,
    margin: 3
  },
  center: {
    justifyContent: "center",
    alignItems: "center"
  },
  number: {
    fontSize: 35,
    color: "#000",
    fontWeight: "200"
  },
  grid: {
    flexDirection: "row",
    flexWrap: "wrap"
  },
  photoStyle: {
    width: "33%",
    height: 150
  },
  userIconImage: {
    height: 25,
    width: 25,
    borderRadius: 15,
    marginRight: 10
  },
  modal: {
    alignItems: "center",
    justifyContent: "center"
  },
  modalContainer: {
    width: "90%",
    maxHeight: "60%"
  },
  header: {
    backgroundColor: "#FFF",
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    overflow: "hidden",
    padding: 8
  },
  footer: {
    backgroundColor: "#FFF",
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    overflow: "hidden",
    padding: 8
  },
  footerContent: {
    justifyContent: "space-around",
    flexDirection: "row"
  },
  image: {
    width: "100%",
    height: "100%"
  },
  text: {
    flex: 1,
    fontSize: 18,
    textAlign: "center"
  },
  bold: {
    fontWeight: "bold"
  }
});
