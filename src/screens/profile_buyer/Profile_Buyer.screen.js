import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TouchableWithoutFeedback
} from "react-native";
import { styles } from "./Profile_Buyer.style";
import Images from './images'

const coinIcon = require("../../assets/coin-icon.png");
const getMargin = Dimensions.get("window").width * 0.05;

class Profile_Buyer extends Component {
  render() {

    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <TouchableOpacity
            style={styles.avatarCoin}
            onPress={() => this.props.navigation.navigate("Poin")}
          >
            <Image source={coinIcon} style={styles.avatarCoin} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.avatar}
            onPress={() => this.props.navigation.navigate("Setting")}
          >
            <View style={styles.avatar} />
            <View style={styles.avatarAdd} />
          </TouchableOpacity>
          <Text style={styles.username}>Umar</Text>
          <View
            style={[
              styles.row,
              { marginHorizontal: getMargin, justifyContent: "space-between" }
            ]}
          >
            <View style={styles.center}>
              <Text style={styles.textSubscriber}>SUBSCRIBER</Text>
              <Text style={styles.number}>10</Text>
            </View>
            <View style={styles.center}>
              <Text style={styles.textPost}>POST</Text>
              <Text style={styles.number}>{Images.length}</Text>
            </View>
            <View style={styles.center}>
              <Text style={styles.textSubscriber}>SUBSCRIBED</Text>
              <Text style={styles.number}>10</Text>
            </View>
          </View>
          <View style={styles.feedStyle}>
            <View style={styles.grid}>
              {Images.map((src, idx) => {
                return (
                  <TouchableWithoutFeedback
                    key={idx}
                    onLongPress={() => this.handleImageOpen(idx)}
                  >
                    <Image
                      source={src}
                      resizeMode="cover"
                      style={styles.photoStyle}
                    />
                  </TouchableWithoutFeedback>
                );
              })}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Profile_Buyer;
