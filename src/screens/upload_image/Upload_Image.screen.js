import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  ActivityIndicator
} from "react-native";
import _ from "lodash";
import { styles } from "./Upload_Image.style";
import ImagePicker from "react-native-image-picker";
import CacheStore from "react-native-cache-store";

const backIcon = require("../../assets/back-icon.png");
import Header from "../../components/Header";
import SwitchSelector from "../../components/SwitchSelector";
import MiniHeader from "../../components/MiniHeader";

class Upload_Image extends Component {
  constructor() {
    super();
    this.state = {
      premium: false,
      convert: false,
      avatarSource: null,
      token: "",
      description: "",
      newValue: "",
      height: 40
    };
  }

  componentDidMount() {
    CacheStore.get("userToken").then(token => this.setState({ token }));
  }

  showImagePicker = () => {
    ImagePicker.showImagePicker(response => {
      console.log(response);
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        // const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: "data:image/jpeg;base64," + response.data };

        this.setState({
          avatarSource: response
        });
      }
    });
  };

  onPostPressed = () => {
    const { premium, convert, token, description, avatarSource } = this.state;
    const image_type = avatarSource.type;
    const image_name = avatarSource.fileName;
    const image_uri = avatarSource.uri;
    this.props.postAction({
      token,
      description,
      convert,
      premium,
      image_type,
      image_name,
      image_uri
    });
  };

  updateSize = height => {
    this.setState({
      height
    });
  };

  render() {
    const { newValue, height } = this.state;

    let newStyle = {
      height
    };

    return (
      <View style={{ flex: 1 }}>
        <Header
          text={true}
          back={backIcon}
          onPress={() => this.props.navigation.goBack()}
          titleRight={
            this.props.loading ? (
              <ActivityIndicator size="large" color={'black'}/>
            ) : (
              <Text style={{ fontSize: 22 }}>POST</Text>
            )
          }
          onPressPost={this.onPostPressed}
          disable={this.state.avatarSource == null}
        />
        <ScrollView style={styles.container}>
          <TouchableOpacity
            style={styles.upload_image}
            activeOpacity={1.7}
            onPress={this.showImagePicker}
          >
            {_.isEmpty(this.state.avatarSource) ? (
              <Text style={styles.textPhoto}>Choose Photo</Text>
            ) : (
              <Image
                source={this.state.avatarSource}
                style={styles.upload_image}
              />
            )}
          </TouchableOpacity>
          <TextInput
            style={[
              newStyle,
              {
                marginLeft: 10,
                fontSize: 20,
                marginRight: 10,
                marginVertical: 10
              }
            ]}
            placeholder="What do you think about your art?"
            value={this.state.text}
            onChangeText={description => this.setState({ description })}
            editable={true}
            multiline={true}
            onContentSizeChange={e =>
              this.updateSize(e.nativeEvent.contentSize.height)
            }
          />
          <View style={{ margin: 10 }}>
            <SwitchSelector
              initial={0}
              onPress={value => this.setState({ premium: value })}
              textColor={"gray"}
              selectedColor={"black"}
              buttonColor={"#c7c7c7"}
              borderColor={"#c7c7c7"}
              hasPadding
              options={[
                { label: "Free art", value: "false" },
                { label: "Premium art", value: "true" }
              ]}
            />
          </View>
          <View style={{ margin: 10 }}>
            <SwitchSelector
              initial={0}
              onPress={value => this.setState({ convert: value })}
              textColor={"gray"}
              selectedColor={"black"}
              buttonColor={"#c7c7c7"}
              borderColor={"#c7c7c7"}
              hasPadding
              options={[
                { label: "Original", value: "false" },
                { label: "Convert to graphite", value: "true" }
              ]}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Upload_Image;
