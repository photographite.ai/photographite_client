import { connect } from "react-redux";
import Upload_Image from "./Upload_Image.screen";

import { postAction } from "../../redux/actions/post";

const mapStateToProps = state => {
  const { loading } = state.post
  return { loading }
}

export default connect(mapStateToProps, { postAction })(Upload_Image);
