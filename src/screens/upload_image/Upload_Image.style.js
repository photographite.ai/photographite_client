import { StyleSheet, Dimensions } from 'react-native';

const getHeight = Dimensions.get('window').height*0.3

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    upload_image: {
        height: getHeight,
        backgroundColor: '#c7c7c7',
        justifyContent: 'center',
        alignItems: 'center'
    },
    place: {
        margin: 8,
        marginBottom: 20,
        borderColor: '#f1f1f1',
        borderWidth: 1,
        height: 40
    },
    textPhoto: {
        color: '#fff',
        fontSize: 30,
        fontWeight: '500'
    },
    placeIcon: {
        height: 20,
        width: 20
    },
    row: {
        flexDirection: 'row',
        marginLeft: 8,
        marginBottom: 3
    },
    imageConvert: {
        height: 40,
        width: 40,
        margin: 8
    },
    button: {
        height: 40,
        width: 120,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: 'blue',
        alignSelf: 'center',
        marginBottom: 10
    },
    textButton: {
        color: '#fff',
        fontSize: 16
    },
    icon: {
        height: 20,
        width: 20,
        position: 'absolute',
        top: 0,
        right: 0,
        margin: 8
    },
    
})