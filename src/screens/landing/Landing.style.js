import { StyleSheet, Dimensions } from 'react-native';

const getWidth = Dimensions.get('window').width*0.5

export const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'center',        
    },
    photographite: {
        color: '#fff',
        fontSize: 45,
        alignSelf: 'center',
        fontFamily: 'yellowtail-regular'
    },
    share: {
        color: '#fff',
        fontSize: 20,
        alignSelf: 'center',
        marginVertical: 8
    },
    button: {
        height: 40,
        width: getWidth,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        alignSelf: 'center'
    },
    buttonText: {
        color: '#fff',
        fontSize: 20,
        fontWeight: '500'
    }
})