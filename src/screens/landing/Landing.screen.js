import React, { Component } from 'react';
import {     
    Text, 
    ImageBackground, 
    TouchableOpacity,
    AsyncStorage
} from 'react-native';
import _ from 'lodash'
import { styles } from './Landing.style';

const landingIcon = require('../../assets/splash-bg.png')

class Landing extends Component {
    render() {
        return (
            <ImageBackground source={landingIcon} style={styles.background}>
                <Text style={styles.photographite}>Photographite</Text>
                <Text style={styles.share}>Share your art, get paid</Text>
                <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('Login')}>
                    <Text style={styles.buttonText}>GET STARTED</Text>
                </TouchableOpacity>
            </ImageBackground>
        )
    }
}

export default Landing;
