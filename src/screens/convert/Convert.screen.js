import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native';
import { styles } from './Convert.style';

const getWidth = Dimensions.get('window').width*0.1
const coinIcon = require('../../assets/coin-icon.png')
const moneyIcon = require('../../assets/money-icon.png')

class Convert extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.row, { marginHorizontal: getWidth }]}>
                    <Image source={coinIcon} style={styles.coin}/>
                    <Text style={styles.arrow}>---></Text>
                    <Image source={moneyIcon} style={styles.money}/>
                </View>
                <TouchableOpacity style={styles.button}>
                    <Text>Convert</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Convert;