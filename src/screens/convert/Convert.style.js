import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    coin: {
        height: 80,
        width: 80,
        margin: 8
    },
    money: {
        height: 80,
        width: 80,
        margin: 8
    },
    button: {
        height: 40,
        width: 150,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#c7c7c7',
        borderRadius: 30,
        alignSelf: 'center',
        marginTop: 20
    },
    arrow: {
        fontSize: 40,
        fontWeight: '500'
    }
})