import { StyleSheet } from "react-native";

const HEADER_HEIGHT = 60

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 8
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 8,
    borderBottomColor: "#e1e6ef",
    borderBottomWidth: 1,
    paddingBottom: 8
  },
  coin: {
    height: 30,
    width: 30,
    marginRight: 8
  },
  textManage: {
    fontSize: 30,
    marginTop: 18,
    alignSelf: "center"
  },
  textHarga: {
    fontSize: 30,
    alignSelf: "center",
    marginBottom: 30
  },
  header: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#303133'
  },
  headerContainer: {
    height: HEADER_HEIGHT,
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingHorizontal: 20,    
  },
  tabContainer: {
    flex: 1,
    paddingVertical: 20
  },
  iconContainer: {
    alignSelf: "center"
  },
  line: {
    borderLeftWidth: 1.5,
    borderColor: "white",
    marginVertical: 18
  },
  icon: {
    color: "#FFF",
    fontSize: 18
  },
  title: {
    color: "white",
    fontSize: 16,
    flex: 1,
    alignSelf: "center"
  }
});
