import React from "react";
import { Animated, Easing, Image } from "react-native";

import {
  createMaterialTopTabNavigator,
  createBottomTabNavigator,
  createStackNavigator
} from "react-navigation";

import Feed from "../screens/feed/Feed.container";
import Profile_Artist from "../screens/profile_artist/Profile_Artist.container";
import Profile_Buyer from "../screens/profile_buyer/Profile_Buyer.container";
import Subscribed from "../screens/subscribed/Subscribed.container";
import Poin from "../screens/poin_screen/Poin_Screen.container";
import Upload_Image from "../screens/upload_image/Upload_Image.container";
import Setting from '../screens/settings/Setting.container';

const feedIcon = require("../assets/home-icon.png");
const subscribedIcon = require("../assets/art-list.png");
const profileIcon = require("../assets/user-icon.png");

const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 200,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true
    },
    screenInterpolator: sceneProps => {
      const { position, layout, scene, index, scenes } = sceneProps;
      const toIndex = index;
      const thisSceneIndex = scene.index;
      const height = layout.initHeight;
      const width = layout.initWidth;

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
        outputRange: [width, 0, 0]
      });

      // Since we want the card to take the same amount of time
      // to animate downwards no matter if it's 3rd on the stack
      // or 53rd, we interpolate over the entire range from 0 - thisSceneIndex
      const translateY = position.interpolate({
        inputRange: [0, thisSceneIndex],
        outputRange: [height, 0]
      });

      const slideFromRight = { transform: [{ translateX }] };
      const slideFromBottom = { transform: [{ translateY }] };

      const lastSceneIndex = scenes[scenes.length - 1].index;

      // Test whether we're skipping back more than one screen
      if (lastSceneIndex - toIndex > 1) {
        // Do not transoform the screen being navigated to
        if (scene.index === toIndex) return;
        // Hide all screens in between
        if (scene.index !== lastSceneIndex) return { opacity: 0 };
        // Slide top screen down
        return slideFromBottom;
      }

      return slideFromRight;
    }
  };
};

const FeedStackNavigator = createStackNavigator(
  { Feed, Profile_Artist },
  { transitionConfig, headerMode: "none" }
);
const SubscribedNavigator = createStackNavigator(
  { Subscribed },
  { transitionConfig, headerMode: "none" }
);


const bottomNavigator = createBottomTabNavigator(
  {
    Feed: {
      screen: FeedStackNavigator,
      navigationOptions: {
        header: null,
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={feedIcon}
            style={[{ height: 20, width: 20 }, { tintColor }]}
          />
        )
      }
    },
    Subscribed: {
      screen: SubscribedNavigator,
      navigationOptions: {
        header: null,
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={subscribedIcon}
            style={[{ height: 20, width: 20, tintColor: '#94989e' }, { tintColor }]}
          />
        )
      }
    },
    Profile: {
      screen: Profile_Buyer,
      navigationOptions: {
        header: null,
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={profileIcon}
            style={[{ height: 15, width: 15 }, { tintColor }]}
          />
        )
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: "#babec4",
      inactiveTintColor: "#4f545b",
      indicatorStyle: { backgroundColor: "red" },
      labelStyle: {
        fontSize: 14
      },
      style: {
        backgroundColor: "#f2f4f7",
        height: 50,
        padding: 0
      },
      showLabel: true
    }
  }
);

const topNavigation = createMaterialTopTabNavigator(
  {
    bottomNavigator: {
      screen: bottomNavigator
    },
    UploadImage: {
      screen: Upload_Image
    },
  },
  {
    tabBarComponent: null,
    swipeEnabled: props => {
      const firstIndex = props.index;
      const firstRoute = props.routes[firstIndex];
      if (firstRoute.routeName === "bottomNavigator") {
        const secoundIndex = firstRoute.index;
        const secoundRoute = firstRoute.routes[secoundIndex];
        if (secoundRoute.routeName === "Feed") {
          return true;
        }
        return false;
      }
      return true;
    }
  }
);

const AuthNavigation = createStackNavigator(
  {
    topNavigation,
    bottomNavigator,
    Setting,
    Poin
  },
  {
    headerMode: "none",
    transitionConfig
  }
)

export default AuthNavigation;
