import React from "react";
import { createStackNavigator } from 'react-navigation'

import Landing from "../screens/landing/Landing.container";
import Login from "../screens/login/Login.container";
import Signup from "../screens/signup/Signup.container";

const UnAuthNavigation = createStackNavigator({
    Login: {
      screen: Login,
      navigationOptions: ({ navigation }) => ({
        header: null,
      })
    },
    Signup: {
      screen: Signup,
      navigationOptions: ({ navigation }) => ({
        header: null,
      })
    },
    Landing: {
        screen: Landing,
        navigationOptions: ({ navigation }) => ({
          header: null,
        })
    },
  }, {
    initialRouteName: 'Landing'
  });
  
export default UnAuthNavigation;
