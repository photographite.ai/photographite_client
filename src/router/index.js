import React from "react";
import {
  createSwitchNavigator
} from "react-navigation";

import UnAuthNavigation from './unAuhorizedNavigation';
import AuthNavigation from './authorizedNavigation';

const RouteConfigs = {
  UnAuthNavigation: {
    screen: UnAuthNavigation
  },
  AuthNavigation: {
    screen: AuthNavigation
  }
}

const SwitchNavigatorConfig = {
  initialRouteName: 'UnAuthNavigation'
}

const ApptNavigation = createSwitchNavigator(RouteConfigs, SwitchNavigatorConfig);

export default ApptNavigation;
