import React from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
const { width } = Dimensions.get("window");

const MiniHeader = ({ backgroundColor, text }) => {
  return (
    <View
      style={{
        backgroundColor: backgroundColor,
        height: 30,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        width,
        position: "absolute",
        top: 60
      }}
    >
      <Text
        style={{
          color: "#fff"
        }}
      >
        {text}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({});

export default MiniHeader;
