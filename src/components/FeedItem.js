import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  Animated,
  Text,
} from "react-native";
import { Icon } from "native-base";
import DoubleTap from './DoubleTap'

const heartIcon = require("../assets/heart.png");

const { width } = Dimensions.get("window");

export default class FeedItem extends Component {
  state = {
    liked: false,
    showMore: true
  };

  onPressLove = () => {
    this.setState({
      liked: !this.state.liked
    });
  };

  onShowMore = () => {
    this.setState({
      showMore: false
    });
  };

  animatedValue = new Animated.Value(0);

  toggleLike = () => {
    this.setState(state => {
      const newLiked = !state.liked;

      if (newLiked) {
        Animated.sequence([
          Animated.spring(this.animatedValue, { toValue: 1 }),
          Animated.spring(this.animatedValue, { toValue: 0 })
        ]).start();
      }

      return { liked: newLiked };
    });
  };

  renderOverlay = () => {
    const imageStyles = [
      styles.overlayHeart,
      {
        opacity: this.animatedValue,
        transform: [
          {
            scale: this.animatedValue.interpolate({
              inputRange: [0, 1],
              outputRange: [0.7, 1.5]
            })
          }
        ]
      }
    ];

    return (
      <View style={styles.overlay}>
        <Animated.Image source={heartIcon} style={imageStyles} />
      </View>
    );
  };

  lasTap = null;

  doubleTapHandler = () => {
    const now = Date.now();
    const delay = 300;

    if (this.lasTap && now - this.lasTap < delay) {
      this.setState({
        liked: !this.state.liked
      });
    } else {
      this.lasTap = now;
    }
  };

  animatedValue = new Animated.Value(0);

  toggleLike = () => {
    this.setState(state => {
      const newLiked = !state.liked;

      if (newLiked) {
        Animated.sequence([
          Animated.spring(this.animatedValue, { toValue: 1 }),
          Animated.spring(this.animatedValue, { toValue: 0 })
        ]).start();
      }

      return { liked: newLiked };
    });
  };

  renderOverlay = () => {
    const imageStyles = [
      styles.overlayHeart,
      {
        opacity: this.animatedValue,
        transform: [
          {
            scale: this.animatedValue.interpolate({
              inputRange: [0, 1],
              outputRange: [0.7, 1.5]
            })
          }
        ]
      }
    ];

    return (
      <View style={styles.overlay}>
        <Animated.Image source={heartIcon} style={imageStyles} />
      </View>
    );
  };

  render() {
    const { title, uri, time, photo, description } = this.props;
    const { liked, showMore } = this.state;
    const buttonShowMore = (
      <Text onPress={this.onShowMore} style={styles.showmore}>
        show more
      </Text>
    );
    return (
      <View style={styles.container}>
        <View style={styles.containerHeader}>
          <View style={styles.headerLeftItem}>
            <View style={styles.logo}>
              <Text style={{ color: "black", fontSize: 12 }}>{photo}</Text>
            </View>
            <View>
              <Text style={styles.title}>{title}</Text>
              {time && time !== "" ? <Text>{time}</Text> : null}
            </View>
          </View>
        </View>
        <DoubleTap onDoubleTap={this.toggleLike}>
          <View>
            <Image
              source={{ uri }}
              style={styles.containerBody}
              resizeMode="cover"
            />
            {this.renderOverlay()}
          </View>
        </DoubleTap>
        <View style={styles.containerFooter}>
          <View style={styles.footerLeftItem}>
            <TouchableOpacity
              style={styles.defaultIcon}
              onPress={this.onPressLove}
            >
              <Icon
                name={`ios-heart${liked ? "" : "-outline"}`}
                style={{ color: liked ? "red" : "#000" }}
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.defaultIcon}>
              <Icon name="ios-chatbubbles-outline" style={{ color: "#000" }} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.containerDescription}>
          <Text style={styles.textTitleBottom}>{title}</Text>
          {showMore ? (
            <Text>
              {description.substr(0, 100)}
              {`...`}
              {buttonShowMore}
            </Text>
          ) : (
            <Text>{description}</Text>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff"
  },
  containerHeader: {
    height: 60,
    width,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  logo: {
    justifyContent: "center",
    alignItems: "center",
    height: 40,
    width: 40,
    borderRadius: 20,
    marginHorizontal: 14,
    borderWidth: 1,
    borderColor: "gray"
  },
  title: {
    fontWeight: "bold",
    color: "#000"
  },
  headerLeftItem: {
    flexDirection: "row"
  },
  buttonMore: {
    padding: 15,
    paddingRight: 25,
    alignSelf: "flex-end"
  },
  containerBody: {
    height: 250,
    width,
    backgroundColor: "#999"
  },
  footerLeftItem: {
    marginTop: 10,
    flexDirection: "row",
    paddingLeft: 9
  },
  defaultIcon: {
    marginHorizontal: 9
  },
  containerDescription: {
    margin: 16,
    marginTop: 8
  },
  textLike: {
    color: "#000",
    fontWeight: "bold",
    marginBottom: 7
  },
  textTitleBottom: {
    color: "#000",
    fontWeight: "bold"
  },
  textDescription: {
    color: "#000"
  },
  showmore: {
    color: "gray"
  },
  iconRow: {
    flexDirection: "row",
    alignSelf: "stretch",
    marginTop: 10,
    paddingVertical: 5,
    paddingHorizontal: 15
  },
  heartIcon: {
    width: 20,
    height: 20
  },
  overlay: {
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  overlayHeart: {
    tintColor: "gray"
  }
});
