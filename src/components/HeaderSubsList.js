import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions
} from "react-native";

const cameraIcon = require("../assets/camera-icon.png");

const { width } = Dimensions.get("window");

const HeaderSubsList = ({ title }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width,
    height: 60,
    backgroundColor: "#fafafa",
    borderBottomWidth: 1,
    borderColor: "#b4b6b7",
    flexDirection: "row",
    alignItems: "center"
  },
  title: {
    marginLeft: 10,
    fontSize: 20,
    color: "#000",
    marginTop: 15,
    marginBottom: 15
  }
});

export default HeaderSubsList;
