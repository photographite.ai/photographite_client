import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions
} from "react-native";

const cameraIcon = require("../assets/camera-icon.png");

const { width } = Dimensions.get("window");

const Header = ({
  camera,
  onPress,
  title,
  back,
  titleRight,
  onPressPost,
  text,
  disable
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.leftItem}>
        <Text style={styles.title}>{title}</Text>
        <TouchableOpacity onPress={onPress} activeOpacity={1.5}>
          <Image source={back} style={{ height: 20, width: 20 }} />
        </TouchableOpacity>
      </View>
      <View style={styles.rightItem}>
        {camera ? (
          <TouchableOpacity onPress={onPress}>
            <Image source={cameraIcon} style={styles.camera} />
          </TouchableOpacity>
        ) : null}
        {text ? (
          <TouchableOpacity onPress={onPressPost} style={styles.postText} disabled={disable}>
            {titleRight}
          </TouchableOpacity>
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width,
    height: 60,
    backgroundColor: "#fafafa",
    borderBottomWidth: 1,
    borderColor: "#b4b6b7",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  leftItem: {
    flexDirection: "row",
    alignItems: "center"
  },
  rightItem: {
    flexDirection: "row",
    alignItems: "center"
  },
  camera: {
    marginRight: 15,
    width: 30,
    height: 30
  },
  title: {
    fontFamily: "yellowtail-regular",
    fontSize: 30,
    marginLeft: 15,
    color: "#000",
    marginTop: 15,
    marginBottom: 15
  },
  postText: {
    marginRight: 25
  }
});

export default Header;
