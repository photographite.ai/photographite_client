import { POST_BEGIN, POST_SUCCESS, POST_FAILURE } from "../constants";
import { postsUrl } from "../../utils/endpoints";
import _ from "lodash";

export const postAction = ({
  token,
  description,
  premium,
  convert,
  image_type,
  image_name,
  image_uri
}) => {
  return dispatch => {
    let data = new FormData();
    const image = {
      uri: image_uri,
      type: image_type,
      name: image_name
    };
    data.append("image", image);
    data.append("story", `${description}`);
    data.append("premium", `${premium}`);
    data.append("convert", `${convert}`);
    console.log(data);
    dispatch({ type: POST_BEGIN });

    return fetch(postsUrl, {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: "Bearer " + `${token}`
      },
      body: data
    })
      .then(res => res.json())
      .then(data => {
        dispatch({ type: POST_SUCCESS, payload: data });
      })
      .catch(error => dispatch({ type: POST_FAILURE, payload: error }));
  };
};
