import {
  GET_FEED_BEGIN,
  GET_FEED_SUCCESS,
  GET_FEED_FAILURE
} from "../constants";

import { feedUrl } from "../../utils/endpoints";

export const feedAction = () => {
  return dispatch => {
    dispatch({ type: GET_FEED_BEGIN });
    return fetch(feedUrl, {
      method: "GET"
    })
      .then(res => res.json())
      .then(data =>
        dispatch({
          type: GET_FEED_SUCCESS,
          payload: data
        })
      )
      .catch(err => dispatch({ type: GET_FEED_FAILURE, payload: err }));
  };
};
