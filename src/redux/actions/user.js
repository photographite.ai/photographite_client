import { ToastAndroid } from "react-native";

import { signUpUrl, signInUrl } from "../../utils/endpoints";
import CacheStore from "react-native-cache-store";

import {
  USERNAME_CHANGED,
  PASSWORD_CHANGED,
  GET_LOGIN_BEGIN,
  GET_LOGIN_FAILURE,
  LOGIN_SUCCESS,
  CONFIRM_PASSWORD,
  CREATE_USER_BEGIN,
  CREATE_USER_SUCCESS,
  PHONE_NUMBER_CHANGED
} from "../constants";

export const usernameChanged = text => {
  return {
    type: USERNAME_CHANGED,
    payload: text
  };
};

export const passwordChanged = text => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export const confirmPasswordChanged = text => {
  return {
    type: CONFIRM_PASSWORD,
    payload: text
  };
};

export const phoneNumberChanged = text => {
  return {
    type: PHONE_NUMBER_CHANGED,
    payload: text
  };
};

export const createUser = ({ username, password, phone }) => {
  return dispatch => {
    dispatch(createUserBegin());
    return fetch(signUpUrl, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        username: username,
        password: password,
        phone: phone
      })
    })
      .then(res => res.json())
      .then(data => {
        dispatch({
          type: CREATE_USER_SUCCESS,
          payload: data
        });
        ToastAndroid.show("anda telah berhasil mendaftar", ToastAndroid.SHORT);
        dispatch({ type: "Navigation/NAVIGATE", routeName: "Login" });
      })
      .catch(err => console.log(err));
  };
};

const createUserBegin = () => {
  return {
    type: CREATE_USER_BEGIN
  };
};

export const postLogin = ({ username, password }) => {
  return dispatch => {
    dispatch({ type: GET_LOGIN_BEGIN });
    return fetch(signInUrl, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        username: username,
        password: password
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data.token) {
          CacheStore.set("userToken", data.token);
          dispatch({
            type: LOGIN_SUCCESS,
            payload: data.token
          });
          ToastAndroid.show("Success !", ToastAndroid.SHORT);
          dispatch({
            type: "Navigation/NAVIGATE",
            routeName: "AuthNavigation"
          });
        } else if (!data.token) {
          dispatch({
            type: GET_LOGIN_FAILURE
          });
          ToastAndroid.show(`Invalid username or password`, ToastAndroid.SHORT);
        }
      })
      .catch(error => {
        dispatch(getLoginFailure(error));
        ToastAndroid.show(`Invalid username or password`, ToastAndroid.SHORT);
      });
  };
};

export const getLoginFailure = error => {
  return {
    type: GET_LOGIN_FAILURE,
    payload: error
  };
};
