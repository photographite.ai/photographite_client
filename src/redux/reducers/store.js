import { createStore, applyMiddleware } from "redux";
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware
} from "react-navigation-redux-helpers";
import { connect } from "react-redux";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";

import ApptNavigation from "../../router";
import appReducer from "./reducers";
import handleBackAndroid from "../../utils/handleBackAndroid";

const navMidleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav
);

const logger = __DEV__ ? createLogger() : () => null;

const App = reduxifyNavigator(ApptNavigation, "root");
const mapStateToProps = state => ({
  state: state.nav
});
const AppWithNavigationState = connect(mapStateToProps)(handleBackAndroid(App));

let createStoreWithMiddleware = null;
if (__DEV__) {
  createStoreWithMiddleware = applyMiddleware(navMidleware, thunk, logger)(
    createStore
  );
} else {
  createStoreWithMiddleware = applyMiddleware(navMidleware, thunk)(createStore);
}

const store = createStoreWithMiddleware(appReducer);

export default () => {
  return { store, AppWithNavigationState };
};
