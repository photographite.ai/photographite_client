import { combineReducers } from "redux";
import nav from "./nav";
import userReducer from "./userReducer";
import post from "./post";
import feed from "./feed";

export default combineReducers({
  nav,
  userReducer,
  post,
  feed
});
