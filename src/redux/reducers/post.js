import { POST_BEGIN, POST_SUCCESS, POST_FAILURE } from "../constants";

const initialState = {
  token: "",
  image_mime: "",
  image_uri: "",
  image_name: "",
  convert: false,
  premium: false,
  story: "",
  data: [],
  loading: false,
  error: ""
};

export default (post = (state = initialState, action) => {
  switch (action.type) {
    case POST_BEGIN:
      return { ...state, loading: true };
    case POST_SUCCESS:
      return { ...state, loading: false, data: action.payload };
    case POST_FAILURE:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
});
