import {
  USERNAME_CHANGED,
  PASSWORD_CHANGED,
  GET_LOGIN_BEGIN,
  GET_LOGIN_FAILURE,
  LOGIN_SUCCESS,
  CONFIRM_PASSWORD,
  CREATE_USER_BEGIN,
  CREATE_USER_SUCCESS,
  USERNAME_EXISTS,
  PHONE_NUMBER_CHANGED
} from "../constants";

const initialState = {
  username: "",
  password: "",
  phone: null,
  rePassword: "",
  token: "",
  error: null,
  loading: false,
  data: [],
  id: null
};

export default (authReducer = (state = initialState, action) => {
  switch (action.type) {
    case USERNAME_CHANGED:
      return { ...state, username: action.payload };
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload };
    case CONFIRM_PASSWORD:
      return { ...state, rePassword: action.payload };
    case PHONE_NUMBER_CHANGED:
      return { ...state, phone: action.payload };
    case GET_LOGIN_BEGIN:
      return { ...state, username: "", password: "", loading: true };
    case LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload,
        loading: false,
        username: "",
        password: ""
      };
    case GET_LOGIN_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
        username: "",
        password: ""
      };
    case CREATE_USER_BEGIN:
      return { ...state, loading: true };
    case CREATE_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        username: "",
        password: "",
        rePassword: "",
        phone: null,
        data: action.payload
      };
    case USERNAME_EXISTS:
      return { ...state, loading: false, username: "", password: "" };
    default:
      return state;
  }
});
