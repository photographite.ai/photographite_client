import {
  GET_FEED_BEGIN,
  GET_FEED_SUCCESS,
  GET_FEED_FAILURE
} from "../constants";

const initialState = {
  loading: false,
  data: []
};

export default function feed(state = initialState, action) {
  switch (action.type) {
    case GET_FEED_BEGIN:
      return { ...state, loading: true };
    case GET_FEED_SUCCESS:
      return { ...state, data: action.payload, loading: false };
    case GET_FEED_FAILURE:
      return { ...state, data: action.payload, loading: false };
    default:
      return state;
  }
}
