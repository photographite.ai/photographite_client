const IP = "192.168.1.126";
export const signUpUrl = `http://${IP}:8000/users/register`;
export const signInUrl = `http://${IP}:8000/users/login`;
export const postsUrl = `http://${IP}:8000/posts`;
export const feedUrl = `http://${IP}:8000/posts/explore`;
