import axios from 'axios'
import CacheStore from 'react-native-cache-store';

import { signUpUrl } from './endpoints';

const urlNotWithToken = {
    registration: signUpUrl
}
const TIME_OUT = 3000

axios.defaults.timeout = TIME_OUT
axios.interceptors.request.use(
    async (response) => {
        const originalConfig = response
        const { url } = response
        if(url === urlNotWithToken ){
            return originalConfig
        }
        const userToken = await CacheStore.get('userToken')
        originalConfig.headers.Authorization = `Bearer ${userToken}`
        return originalConfig
    },
    error => Promise.reject(error)
)


export default axios;
